package sample1;

public class SoftwareSales {
	
	public double calculatePrice(int quantity) {
		
		double subtotal = 99* quantity;
		double discount= 0.0;
		
		if(quantity >= 10 && quantity <=19)
		{
			
			discount = subtotal*0.2;
			}
		else if(quantity >= 19 && quantity <=49)
		{
			
			discount = subtotal*0.3;
			
		}
		else if(quantity >= 50 && quantity <=99)
		{
			
			 discount = subtotal*0.4;
			
		}
		else if(quantity >= 100)
		{
			
			 discount = subtotal*0.5;
			
		}
		
		
		else if(quantity < 0)
		{
			
			return -1;
		}
		
		double finalTotal = subtotal- discount;
		return finalTotal;
		
	
		
	}

}
