package sample1;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SoftwareSalesTest {
	SoftwareSales s;
	@Before
	public void setUp() throws Exception {
		s = new SoftwareSales();
	}

	@After
	public void tearDown() throws Exception {
	}
//R1- buy one software package 
	@Test
	public void testBuyOneSoftwarePckage() {
		
		double finalPrice = s.calculatePrice(1);
	
		assertEquals(99, finalPrice,0);
		
	}

	@Test
	//R2- buy 10-19 package, get20%
	public void testBuy10SoftwarePckage() {
		double finalPrice = s.calculatePrice(12);
	
		assertEquals(950.4, finalPrice,0);
	
	
	}
	@Test
	//R3- buy 20-49 package, get30%
	public void testBuy20SoftwarePckage() {
		double finalPrice = s.calculatePrice(30);
	
		assertEquals(2079, finalPrice,0);
	}
	@Test
	//R4- buy 50-99 package, get40%
		public void testBuy50SoftwarePckage() {
			double finalPrice = s.calculatePrice(60);
		
			assertEquals(3564, finalPrice,0);
		}
	@Test
	//R5- buy 100-+ package, get40%
	public void testBuy100SoftwarePckage() {
		double finalPrice = s.calculatePrice(120);
	
		assertEquals(5940, finalPrice,0);
	}
	@Test
	//R6- buy -ve package, get40%
	public void testBuyNegSoftwarePckage() {
		double finalPrice = s.calculatePrice(-10);
	
		assertEquals(-1, finalPrice,0);
	}
	@Test
	//R6- buy 0package, get40%
	public void testBuyZeroSoftwarePckage() {
		double finalPrice = s.calculatePrice(0);
	
		assertEquals(0, finalPrice,0);
	}	
}
